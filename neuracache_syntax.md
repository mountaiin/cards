one liner? : doppelpunkt answer tag #flashcard #neura

regular card? tag #flashcard #neura
answer first line
answer can also have image, latex, code etc
'- - ...'

- - -

pattern of regular card? #flashcard #neura
question tag
answer
'- - .. or * * .. or **.'

- - - 

how to group cards? #flashcard #neura
question : answer tag flshcrd tag abc
question : answer tag flshcrd tag abc tag xyz

regular card question tag flshcrd tag xyz
answer regular card
answer regular card
'- - ..'
'- - ..'

how to tag grouping? #flashcard
tag 'tags' tagA tagB tagC

eg question1 tag flshcrd
answer1
'- - ..'

question2 tag flshcrd
answer2

- - -

pro version of group tagging #flashcard #neura
tag 'tags' 'tagA/tagB/tagC'

question1 tag flshcrd
answer1

question2 tag flshcrd
answer2

question3 tag flshcrd
answer3
'- - ..'

- - -

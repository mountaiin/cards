hallo welt

- - -

basic vim command #flashcard #vim
to quit -> :q
quit without saving changes -> :q!
to save something -> :w
save and quit -> :wq

- - -

set numbers #flashcard #vim
:set number

- - -

change to insert mode #flashcard #vim
i

- - -

command mode #flashcard #vim
escape

- - -

#tags #vim #shortcuts

delete line #flashcard
dd

delete n lines #flashcard
ndd

undo & redo #flashcard
u
^r

- - -

search strings in text & go forward #flashcard
/string
n OR N

- - -

replace strings #flashcard
:%s/string_to_be_replaced/with_this_string

- - -



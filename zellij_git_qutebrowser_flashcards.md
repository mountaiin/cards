zellij options #flashcard #cli #zellij
-s [name] start new session
a [name] attach to existing session
ls list active sessions

- - -

#tags #cli #git

three levels of setup? #flashcard
git config --system
git config --global
git config --local 

show settings OR all git configs? #flashcard
git config --list
git config --list --show-origin

git set identity? #flashcard
git config --global user.name "john doe"
git config --global user.email johndoe@example.com

git set editor? #flashcard
git config --global core.editor nvim

- - -

qutebrowser cht.sh web inspect, open url, navi? #flashcard #qutebrowser
wf
o
for- backward [[ ]], up/down ^b ^f, halfpageUp/Down ^u ^d

- - -

### from ipad

touch README.md
git init
git checkout -b main
git add README.md
git commit -m "inital commit"
git remote add origin httP:// [...] .git
git push -u origin main